{{ 
    config(
    pre_hook=['set foreign_key_checks=OFF','truncate table {{ this }}'],
    materialized='incremental',
    enabled=true) 
}}
select
kode as id,
company.id as company_id,
ifnull(kode_anggota,kode) as customer_code,
null as nip,
nama as customer_name,
CASE 
	when jenis_kelamin = 'L' then 1
	when jenis_kelamin = 'P' then 0
    else 1
END as customer_gender,
ifnull(tanggal_lahir,"1971-01-01") as customer_birth_date,
tempat_lahir as customer_birth_place,
jenis_nasabah as customer_type_id,
ifnull(telp,"") as customer_phone,
alamat as customer_address,
email as customer_email,
0 as customer_pin,
ibu_kandung as customer_mother_name,
ahli_waris as customer_heir_name,
status_nasabah as customer_status,
tanggal_daftar_calon_anggota as customer_date,
profesion.id as profession_id,
religion.id as religion_id,
identity_type.id as identity_id,
marital.id as marital_id,
kode_area as area_id,
npwp as tax_identify_number,
ifnull(no_identitas,"") as customer_identity_number,
null as customer_referal_type,
null as customer_referal_id,
null as customer_referal_code,
1 as created_by,
1 as updated_by,
null as deleted_at,
now() as created_at,
now() as updated_at,
ifnull(shu,0) as customer_dividend,
null as saving_id_dividend,
null as customer_company_name,
unit_nasabah as customer_unit_id,
null as saving_interest_tax_return_id,
0 as customer_is_blocked
from {{ source('template', 'customers')}} c 
left join (select id,company_code from sbs_tms.m_companies mc) company
on c.kode_cabang COLLATE utf8mb4_general_ci = company.company_code
left join (select id,profession_name from sbs_tms.m_cb_professions mcp where mcp.deleted_at is null group by profession_name) profesion
on c.pekerjaan COLLATE utf8mb4_general_ci = profesion.profession_name
left join (select id, religion_name from sbs_tms.m_cb_religions mcr where mcr.deleted_at is null) religion
on c.agama COLLATE utf8mb4_general_ci = religion.religion_name
left join (select id,identity_type_name from sbs_tms.m_cb_identity_types mcit where mcit.deleted_at is null) identity_type
on c.jenis_identitas COLLATE utf8mb4_general_ci = identity_type.identity_type_name
left join (select id, marital_name from sbs_tms.m_cb_marital_status mcms where mcms.deleted_at is null) marital
on c.status_pernikahan COLLATE utf8mb4_general_ci = marital.marital_name