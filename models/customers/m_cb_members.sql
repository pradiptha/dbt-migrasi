{{ 
    config(
    materialized='incremental',
    enabled=true) 
}}
-- depends_on: {{ref('m_cb_customers')}}
select
@n := @n + 1 as id,
kode_anggota as member_code,
tanggal_daftar_anggota as member_date,
kode as customer_id,
kode as customer_code,
1 as created_by,
1 as updated_by,
null as deleted_at,
now() as created_at,
now() as updated_at
from {{ source('template','customers')}} c
,(SELECT @n := (select ifnull(max(id),1) from sbs_tms.m_cb_members)) m
where c.status_anggota = 1