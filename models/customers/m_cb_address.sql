{{ 
    config(
    materialized='incremental',
    pre_hook=["delete from {{this}} where source_type = 'Modules\\Banking\\Entities\\Customer'"],
    enabled=true) 
}}
-- depends_on: {{ref('m_cb_customers')}}
select 
@n := @n + 1 as id,
'Modules\\Banking\\Entities\\Customer' as source_type,
kode as source_id,
null as county_id,
null as city_id,
null as district_id,
null as subdistrict_id,
1 as address_type_id,
ifnull(alamat,"") as address_street,
0 as address_latitude,
0 as address_longitude,
1 as address_default,
1 as created_by,
1 as updated_by,
now() as created_at,
now() as updated_at,
null as deleted_at
from {{ source('template','customers')}} c 
,(SELECT @n := (select ifnull(max(id),1) from sbs_tms.m_cb_address)) m