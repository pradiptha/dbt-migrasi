{{ 
    config(
    materialized='incremental',
    pre_hook=['set global foreign_key_checks=off','truncate table {{this}}'],
    enabled=true) 
}}
-- depends_on: {{ref('m_cb_customers')}}
select
@n := @n + 1 as id,
kode as customer_id,
kode as customer_code_before,
kode as customer_code_after,
0 as customer_member_stop,
0 as customer_member_status,
'Migrasi - Menjadi Calon Anggota' as history_desc,
1 as created_by,
now() as created_at
from {{ source('template','customers')}} c
,(SELECT @n := (select ifnull(max(id),1) from sbs_tms.m_cb_address)) m

union all

select
@n := @n + 1 as id,
kode as customer_id,
kode as customer_code_before,
kode_anggota as customer_code_after,
0 as customer_member_stop,
1 as customer_member_status,
'Migrasi - Menjadi Anggota' as history_desc,
1 as created_by,
NOW() + INTERVAL 1 SECOND as created_at
from {{ source('template','customers')}} m
where m.status_anggota = 1

union all

select
@n := @n + 1 as id,
kode as customer_id,
kode as customer_code_before,
kode as customer_code_after,
1 as customer_member_stop,
0 as customer_member_status,
'Migrasi - Berhenti' as history_desc,
1 as created_by,
NOW() + INTERVAL 2 SECOND as created_at
from {{ source('template','customers')}} c
where tanggal_berhenti is not null