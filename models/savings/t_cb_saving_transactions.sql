{{ 
    config(
    pre_hook=['set global foreign_key_checks=OFF','truncate table {{ this }}'],
    materialized='incremental',
    enabled=true) 
}}
-- depends_on: {{ ref('t_cb_saving_close') }}
select
LOWER(CONCAT(
    LPAD(HEX(FLOOR(RAND() * 0xffff)), 4, '0'), 
    LPAD(HEX(FLOOR(RAND() * 0xffff)), 4, '0'), '-',
    LPAD(HEX(FLOOR(RAND() * 0xffff)), 4, '0'), '-', 
    '4',
    LPAD(HEX(FLOOR(RAND() * 0x0fff)), 3, '0'), '-', 
    HEX(FLOOR(RAND() * 4 + 8)), 
    LPAD(HEX(FLOOR(RAND() * 0x0fff)), 3, '0'), '-', 
    LPAD(HEX(FLOOR(RAND() * 0xffff)), 4, '0'),
    LPAD(HEX(FLOOR(RAND() * 0xffff)), 4, '0'),
    LPAD(HEX(FLOOR(RAND() * 0xffff)), 4, '0'))) as id,
company.id as company_id,
kode_transaksi as transaction_code,
kode_simpanan as saving_id,
collector.id as collector_id,
transaction_type.id as transaction_type_id,
null as transaction_source_id,
null as transaction_source_type,
tanggal_transaksi as transaction_date,
waktu_transaksi as transaction_time,
periode_transaksi_tabungan_berjangka as transaction_period,
deskripsi as transaction_desc,
deskripsi as transaction_remark,
saldo_awal_transaksi as transaction_begin_balance,
debit as transaction_debit,
kredit as transaction_credit,
pajak as transaction_tax,
saldo_akhir as transaction_end_balance,
0 as transaction_correction,
0 as transaction_interest_taxed,
0 as transaction_income,
0 as transaction_remaining,
0 as transaction_fund_allowance,
0 as saving_income_type,
0 as transaction_print_order,
no_urut_transaksi as transaction_number,
0 as transaction_schedule,
NULL as transaction_serial_number,
1 as created_by,
1 as updated_by,
now() as created_at,
now() as updated_at,
null as deleted_at,
null as transaction_date_collect
from {{ source('template','saving_transactions')}}  st
left join (select id,company_code from sbs_tms.m_companies mc) company
on st.kode_cabang COLLATE utf8mb4_general_ci = company.company_code
left join (select id,collector_code from sbs_tms.m_cb_collector mcc where mcc.deleted_at is null) collector
on st.kode_kolektor COLLATE utf8mb4_general_ci = collector.collector_code
left join (select id,transaction_type_code from sbs_tms.m_cb_transaction_types mctt where mctt.deleted_at is null) transaction_type
on st.tipe_transaksi COLLATE utf8mb4_general_ci = transaction_type.transaction_type_code
where year(tanggal_transaksi) >= 2022