{{ 
    config(
    pre_hook=['set global foreign_key_checks=OFF','truncate table {{ this }}'],
    materialized='incremental',
    enabled=true) 
}}
-- depends_on: {{ ref('t_cb_saving_accounts') }}
select
@n := @n + 1 as id,
company.id as company_id,
kode_simpanan as saving_id,
tanggal_tutup_rekening as saving_close_date,
'Migrasi - Tutup Rekening' as saving_close_desc,
0 as saving_close_balance,
1 as created_by,
1 as updated_by,
now() as created_at,
now() as updated_at,
null as deleted_at
from {{ source('template','saving_accounts')}} sa
left join (select id,company_code from sbs_tms.m_companies mc) company
on sa.kode_cabang COLLATE utf8mb4_general_ci = company.company_code
,(SELECT @n := (select ifnull(max(id),1) from sbs_tms.t_cb_saving_close)) m
where sa.status = 0