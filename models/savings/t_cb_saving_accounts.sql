{{ 
    config(
    pre_hook=['set global foreign_key_checks=OFF','truncate table {{ this }}'],
    materialized='incremental',
    enabled=true) 
}}
-- depends_on: {{ ref('m_cb_customers') }}
select
kode_simpanan as id,
company.id as company_id,
kode_simpanan as saving_code,
kode_nasabah as customer_id,
collector.id as collector_id,
tanggal_pembuatan as saving_date,
saving_type.id as saving_type_id,
bunga as saving_interest,
0 as saving_basic_interest,
saldo as saving_balance,
0 as saving_remaining,
periode_simpanan_berjangka as saving_time_period,
jatuh_tempo as saving_due_date,
perpanjangan_otomatis as saving_auto_renewal,
simpanan_wajib as saving_mandatory,
if(status = 1 , 1, 0) as saving_status,
simpanan_berjangka as saving_has_period,
0 as saving_period_total_interest,
0 as saving_interest_balance,
null as saving_interest_balance_date,
rekening_sumber as saving_id_origin,
null as saving_account_data,
null as saving_id_period,
null as saving_serial_number,
1 as created_by,
1 as updated_by,
null as deleted_at,
now() as created_at,
now() as updated_at,
saldo as temp_balance
from {{ source('template','saving_accounts')}} sa
left join (select id,company_code from sbs_tms.m_companies mc) company
on sa.kode_cabang COLLATE utf8mb4_general_ci = company.company_code
left join (select id,collector_code from sbs_tms.m_cb_collector mcc where mcc.deleted_at is null) collector
on sa.kode_kolektor COLLATE utf8mb4_general_ci = collector.collector_code
left join (select id,saving_type_code from sbs_tms.m_cb_saving_types mcst where mcst.deleted_at is null) saving_type
on sa.kode_produk_simpanan COLLATE utf8mb4_general_ci = saving_type.saving_type_code